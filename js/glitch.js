var italic = false;

function toggleItalic(){
	
	if(!italic){
		// $('#tg-text').css('font-style', 'italic');
		var angle = Math.round(Math.random() * (12 - 3)) + 3;
		$('#tg-text').css('transform', 'skewX(-'+angle+'deg) translateX('+angle+'px)');
	}else{
		// $('#tg-text').css('font-style', 'normal');
		$('#tg-text').css('transform', 'skewX(0deg) translateX(0px)');
	}
	
	italic = !italic;
}

(function loop() {
    var rand = Math.round(Math.random() * (2000 - 500)) + 500;
    setTimeout(function() {
            toggleItalic();
            loop();  
    }, rand);
}());