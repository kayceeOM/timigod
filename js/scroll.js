$('#see-more').click(function(){
	$('html, body').animate({
		scrollTop: $('.next').offset().top
	});
});

$('#scroll-to-top').click(function(){
	$('html, body').animate({
		scrollTop: 0
	});
});


var offset, next, scrollToTop;

$(function(){
	// next = $('.next');
	scrollToTop = $('#scroll-to-top');
	offset = $('.next').offset().top;
});

$(window).scroll(function(){
	
	if($(window).scrollTop() > offset/2){
		scrollToTop.css('opacity', 1);
	}else{
		scrollToTop.css('opacity', 0);
	}
});