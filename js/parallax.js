// initialize vars for parallax effect in #hero
function getScrollPercent(val) {
    var h = document.documentElement,
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';
    return h[st] || b[st] / ((h[sh] || b[sh]) - h.clientHeight) * val;
}