$('.prompt#expand').click(function(e){
	e.preventDefault();
	$('.full-bio').removeClass('collapsed');
	$(this).addClass('hidden');
	$('.prompt#collapse').removeClass('hidden');
});

$('.prompt#collapse').click(function(e){
	e.preventDefault();
	$('.full-bio').addClass('collapsed');
	$(this).addClass('hidden');
	$('.prompt#expand').removeClass('hidden');

	//scroll to top of .about here
	$('html, body').animate({
		scrollTop: $('#about').offset().top
	});
});